import pygame
import random 
pygame.init()

screen=pygame.display.set_mode((500,500))
pygame.display.set_caption("Movimiento con las teclas")
colorFondo= (1,150,70)
colorFigura= (255,255,255)

velocidad= 15
runing=True 
Xpos=random.randint(1,500)
Ypos=random.randint(1,300)
r=random.randint(0,200)
k=random.randint(0,500)
h=random.randint(0,500)
while True:
    screen.fill(colorFondo)
    pygame.draw.circle(screen,colorFigura,(Xpos,Ypos,50,50),(h,k,20,20),(r,5))
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            runing=False
            
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                Xpos -= velocidad
                if Xpos < 0:  
                    Xpos=0
            elif event.key == pygame.K_d:
                Xpos += velocidad
                if Xpos>(600-50):
                    Xpos=600-50
            elif event.key == pygame.K_w:
                Ypos -= velocidad
                if Ypos<0:
                    Ypos=0
            elif event.key == pygame.K_s:
                Ypos += velocidad
                if Ypos>(400-50):
                    Ypos=400-50
    pygame.display.update()
    pygame.display.update()              
        